/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/


//=============================================================================
//
//  CLASS QtFunctionPlot - IMPLEMENTATION
//
//=============================================================================

//== INCLUDES =================================================================

#include "QChartsPlot.hh"

#include <QMessageBox>
#include "QChartView"
#include "QValueAxis"
#include "QBarSet"
#include "QRubberBand"
#include "QStackedBarSeries"
#include "TextureMath.hh"

#include <iostream>
#include <algorithm>
#include <vector>
#include <cfloat>
#include <cmath>

#include <ACG/Utils/ColorCoder.hh>

//== NAMESPACES ===============================================================

//== IMPLEMENTATION ==========================================================
/// Default constructor
QChartsPlot::QChartsPlot(QWidget* _parent) :
    QChartView( _parent ),
    Ui::QChartsPlotBase(),
    min_(FLT_MAX),
    max_(FLT_MIN)
{
  setupUi( this );

  // delete widget on close
  setAttribute(Qt::WA_DeleteOnClose, true);

  //create chart
  chart_ = new QChart();
  
  //create x-axis 
  axisX = new QValueAxis();
  axisX->setTitleText("Values");
  axisX->setRange(0, 100);
  chart_->addAxis(axisX, Qt::AlignBottom);

  // Create y-axis
  axisY = new QValueAxis();
  axisY->setTitleText("Count");
  axisY->setRange(0, 500);
  chart_->addAxis(axisY, Qt::AlignLeft);

  // Set the chart to the chartView(this widget)
  setChart(chart_);
  setRubberBand(QChartView::RectangleRubberBand);

  // Create the series and attach it to the chart
  series = new QStackedBarSeries();
  chart_->addSeries(series);
  series->attachAxis(axisY);

  // Get the rubberband from the chartView
  rubberBand_ = findChild<QRubberBand *>();
  rubberBand_->installEventFilter(this);

  // Install eventfilter for the chartView, so we can update the cursor type if inside the chart 
  this->installEventFilter(this);

  // Connect the rubberband signal to the slot
  connect(this, &QChartsPlot::rubberBandChanged, this, &QChartsPlot::rubberBandChangedSlot);
  
  // Set the render hint to antialiasing, so the chart looks nicer
  setRenderHint(QPainter::Antialiasing);

  // Hide the legend
  chart_->legend()->setVisible(false);

  image_ = 0;
}

//------------------------------------------------------------------------------

// Override the mouseReleaseEvent to emit a signal when the right mouse button is released 
void QChartsPlot::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::RightButton) {
       // There might be a more elegant way to reset the zoom, but this is simple and works well
       replot();
    }
    else {
       QChartView::mouseReleaseEvent(event);
    }
}

//------------------------------------------------------------------------------

// Event filter
bool QChartsPlot::eventFilter(QObject *obj, QEvent *event)
{
    if (obj == this && event->type() == QEvent::Enter){
        // if we are inside the chart, we wanna change the cursor to a cross
        setCursor(Qt::CrossCursor);
    }
    else if (obj == chart_ && event->type() == QEvent::Leave){
        // if we leave the chart, we wanna change the cursor back to the default
        setCursor(Qt::ArrowCursor);
    }

    // if we use rubberband on the chartView, we wanna update the x-axis values (as the x-axis is detached from the series)
    if (obj == rubberBand_ && (event->type() == QEvent::HideToParent)){
        //transform the rubberband coordinates to the x-axis values
        auto rubberminX = rubberBand_->geometry().x() - chart_->plotArea().x();
        auto rubbermaxX = rubberBand_->geometry().x() - chart_->plotArea().x() + rubberBand_->geometry().width();
        
        emit rubberBandChanged(rubberminX, rubbermaxX);
    } 
    return false;

}

//------------------------------------------------------------------------------

void QChartsPlot::rubberBandChangedSlot(double rubberminX, double rubbermaxX)
{

  //get the x values of the chart
  qreal minX = static_cast<const QValueAxis*>(chart_->axes(Qt::Horizontal).back())->min();
  qreal maxX = static_cast<const QValueAxis*>(chart_->axes(Qt::Horizontal).back())->max();
  
  //compute the new x-axis values
  double newMin = minX + ((rubberminX)/ chart_->plotArea().width()) * (maxX - minX);
  double newMax = minX + (rubbermaxX / chart_->plotArea().width()) * (maxX - minX);

  //set the new x-axis values
  dynamic_cast<QValueAxis*>(chart_->axes(Qt::Horizontal).back())->setRange(newMin, newMax);

}

//------------------------------------------------------------------------------

void QChartsPlot::setParameters(const TexParameters& _parameters)
{
  parameters_ = _parameters;
}

//------------------------------------------------------------------------------

void QChartsPlot::setParameters(
    bool   _repeat,
    double _repeatMax,
    bool   _clamp,
    double _clampMin,
    double _clampMax,
    bool   _center,
    bool   _absolute,
    bool   _scale)
{

  parameters_.repeat    = _repeat;
  parameters_.repeatMax = _repeatMax;
  parameters_.clamp     = _clamp;
  parameters_.clampMin  = _clampMin;
  parameters_.clampMax  = _clampMax;
  parameters_.center    = _center;
  parameters_.abs       = _absolute;
  parameters_.scale     = _scale;
}

//------------------------------------------------------------------------------

void QChartsPlot::setFunction(const std::vector<double>& _values)
{
  values_ = _values;
}

//------------------------------------------------------------------------------

void QChartsPlot::setImage(QImage* _image)
{
  image_ = _image;
}

//------------------------------------------------------------------------------

void QChartsPlot::replot()
{
    // Create intervals
    int intervalCount = 101;

    std::vector<int> intervals(intervalCount, 0);

    double raw_Min = FLT_MAX;
    double raw_Max = -FLT_MAX;

    // Compute Min and Max values of the raw input values
    for (const auto& value : values_) {
        if (!std::isnan(value)) { // Check for NaN
            raw_Min = std::min(raw_Min, value);
            raw_Max = std::max(raw_Max, value);
        }
    }

    // TextureMath object for value transformation
    TextureMath convert(parameters_, raw_Min, raw_Max); 

    // Create new values
    std::vector<double> new_values(values_.size());

    // transform all values according to the parameters
    for (size_t i = 0; i < values_.size(); i++){ 
      new_values[i] = convert.transform_qtcharts(values_[i]);
    }

    //compute Min and Max of the transformed values 
    double transformed_Min = FLT_MAX;
    double transformed_Max = -FLT_MAX;

    for (const auto& value : new_values) {
        if (!std::isnan(value)) { // Check for NaN
            transformed_Min = std::min(transformed_Min, value);
            transformed_Max = std::max(transformed_Max, value);
        }
    } 

    // This is a special case, where all values are the same
    // In this case we want to create some Margins around the value
    // As the Value will not be displayed 100% correctly in the histogram
    // We also want to print it 
    if (raw_Max == raw_Min){
      std::cout << "All values are the same: " << raw_Max << std::endl;
      raw_Max = raw_Min + 50;
      raw_Min = raw_Min - 50;
    }

    // Compute the width of the intervals
    double width = (raw_Max - raw_Min) / intervalCount;

    // if center is enabled, we want to center the intervals. 
    float shift = 0.0;
    if (parameters_.center){
      //compute the shift
      //this shift is applied to the values, such that the intervals are centered in the histogram
      float center_clipped = (transformed_Max + transformed_Min) / 2.0;
      float center_raw = (raw_Max + raw_Min) / 2.0;
      shift = center_raw - center_clipped;
    }
    
    // Populate the intervals
    for (const auto& value : new_values) {
        if (!std::isnan(value)) { // Check for NaN
            int index = std::min(intervalCount-1, static_cast<int>((value + shift - raw_Min) / width));
            intervals[index]++;
        }
    }

    // Create Colors for the intervals based on the image_
    std::vector<QColor> colors;
    if (image_ != 0){
      for (int i = 0; i < intervalCount; i++) {
        const double intervalCenter = raw_Min + (i + 0.5) * width;
        const double value = convert.transform(intervalCenter);
        int val = int(value * (image_->width() - 1)) % image_->width(); // Simulate repetition
        QColor color = (image_ != nullptr) ? QColor(image_->pixel(val, 0)) : Qt::black; // Image should never be nullptr, but just in case
        colors.push_back(color);
      }
    } 

    // Remove the old series and create a new one. For some reason you have to remove the series. Simply clearing the series and appending new values does not work.
    chart_->removeSeries(series);

    // Clear the series
    series = new QStackedBarSeries();

    // Set the axes range
    axisX->setRange(raw_Min-shift, raw_Max-shift);
    axisY->setRange(0, *std::max_element(intervals.begin(), intervals.end()));

    // Create for each Bar a new set. This is needed to add different colors to each bar.
    for (int i = 0; i < intervalCount; i++) {
        QBarSet* set = new QBarSet("Values");
        for (int j = 0; j < intervalCount; j++) {
            if (j == i) {
                *set << intervals[j];
                if (image_ != 0){
                  set->setColor(colors[j]);
                } 
                
            } else {
                *set << 0;
            }
        }
        series->append(set);
    }
    chart_->addSeries(series);
    series->attachAxis(axisY);

    // If scaling is enabled, zoom into the clamped range
    // Setting the range is not enough again, because the x-axis is detached from the series
    // We need to zoom in the chart and set the x-axis values
    if (parameters_.clamp && parameters_.scale){

       //convert axis values to the pixel values for the zoom
       double minX = (transformed_Min-raw_Min + shift)/(raw_Max-raw_Min ) * chart_->plotArea().width();
       double maxX = (transformed_Max-raw_Min + shift)/(raw_Max-raw_Min) * chart_->plotArea().width();

       //safety margin to make sure the entire bar is visible at the edges 
       double safetyMargin = 10;
       
       //zoom 
       chart_->zoomIn(QRectF(minX + chart_->plotArea().x()-safetyMargin, 0 + chart_->plotArea().y(), maxX-minX+safetyMargin*2, chart_->plotArea().height()));

       //convert safety margin to axis values
       double safetyMarginAxis = safetyMargin / chart_->plotArea().width() * (transformed_Max - transformed_Min);

       //set the x-axis values accordingly
       dynamic_cast<QValueAxis*>(chart_->axes(Qt::Horizontal).back())->setRange(transformed_Min-safetyMarginAxis, transformed_Max+safetyMarginAxis);
    }
}

//=============================================================================
//=============================================================================
