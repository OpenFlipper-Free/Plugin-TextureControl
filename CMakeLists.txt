include(plugin)

openflipper_plugin( OPTDEPS OpenVolumeMesh 
                    TYPES POLYMESH TRIANGLEMESH
                    OPT_TYPES BSPLINESURFACE HEXAHEDRALMESH POLYHEDRALMESH
                    DIRS Dialogs 
                  )
